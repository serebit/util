package exosoft.util;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

public class XMLParser {
    public static Document parse(String path) throws IOException {
        return parse(new File(path));
    }
    
    public static Document parse(File file) throws IOException {
        Document doc = null;
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            doc = db.parse(file);
            doc.normalize();
        } catch (ParserConfigurationException | SAXException e) {
            e.printStackTrace();
        }
        return doc;
    }
}