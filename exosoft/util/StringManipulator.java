package exosoft.util;

/**
 * Created by campbell on 7/4/16.
 */
public class StringManipulator {
    public static String[] splitstrip(String s, String sub) {
        String[] strings;
        if (s.contains(sub)) {
            strings = s.split(sub);
        } else {
            strings = new String[1];
            strings[0] = s;
        }
        for (int i = 0; i < strings.length; i++) {
            strings[i] = strings[i].replaceAll("\\s+","");
        }
        return strings;
    }
}